#!/bin/bash

echo "Setting firewall rules"
iptables -t nat -A POSTROUTING -j MASQUERADE
iptables -t nat -A PREROUTING -p tcp --dport 4000 -j DNAT --to-destination $(dig +short server):4000

iptables -t nat -vL POSTROUTING
iptables -t nat -vL PREROUTING

sleep infinity